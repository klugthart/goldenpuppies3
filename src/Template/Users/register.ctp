
<div class="form-signin">
    <?= $this->Form->create($user); ?>
    <h2 class="form-signin-heading"><?= __('Register') ?></h2>
        <?php
            echo $this->Form->input(
                'username', [
                    'placeholder' => 'Email address'
            ]);
            echo $this->Form->input(
                'password', [
                    'placeholder' => 'Password'
            ]);
            echo $this->Form->input(
                'confirm password', [
                    'type'=>'password',
                    'placeholder' => 'Confirm email address'
            ]);
            echo $this->Form->input('role', [
                'options' => ['admin' => 'Admin', 'customer' => 'Customer', 'guest' => 'Guest']
            ]);
        ?>
    
    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-lg btn-primary btn-block']) ?>
    <?= $this->Html->Link(__('Sign in'), '/login', ['class' => 'btn btn-lg btn-secondary btn-block']); ?>
    <?= $this->Form->end() ?>
</div>
