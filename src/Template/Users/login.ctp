<!-- File: src/Template/Users/login.ctp -->
<?= $this->Flash->render('auth') ?>
<?= $this->Form->create('Users',['class' => 'form-signin']) ?>

	
	<h2 class="form-signin-heading">Please sign in</h2>
	<?php 
	$this->Form->templates([
	    'inputContainer' => '{{content}}'
	]);
	?>
	<?= $this->Form->input(
		'username', [
			'type' => 'email',
			'class' => 'form-control',
			'placeholder' => 'Email address',
			'label' => ['class' => 'sr-only'],
			'required' => true
		]
	) ?>
	<?= $this->Form->input(
		'password', [
			'class' => 'form-control',
			'placeholder' => 'Password',
			'label' => ['class' => 'sr-only'],
			'required' => true
		]
	) ?>
	<div class="checkbox">
		<label>
		<input type="checkbox" value="remember-me"> Remember me
		</label>
	</div>
	<?= $this->Form->button(__('Sign in'), ['class' => 'btn btn-lg btn-primary btn-block']); ?>
	<?= $this->Html->Link(__('Register'), '/register', ['class' => 'btn btn-lg btn-secondary btn-block']); ?>
<?= $this->Form->end() ?>