	<?= $this->Html->image('logo.jpg'); ?>
<div class="blog-masthead">
	<div class="container">
		<nav class="blog-nav">
			<a class="blog-nav-item active" href="#">Home</a>
			<?php
				echo $this->Html->link(__('The dogs'),['controller'=>'pages', 'action'=>'dogs'],['class'=>'blog-nav-item']);
				echo $this->Html->link(__('The humans'),['controller'=>'pages', 'action'=>'humans'],['class'=>'blog-nav-item']);
				echo $this->Html->link(__('Puppies for sale'),"#",['class'=>'blog-nav-item']);
				echo $this->Html->link(__('Buying a pup'),"#",['class'=>'blog-nav-item']);
				echo $this->Html->link(__('Photos'),"#",['class'=>'blog-nav-item']);
				echo $this->Html->link(__('Applause'),"#",['class'=>'blog-nav-item']);
				echo $this->Html->link(__('Contact Us'),"#",['class'=>'blog-nav-item']);
				echo $this->Html->link(__('Blog / Updates'),"#",['class'=>'blog-nav-item']);

			?>
		</nav>
	</div>
</div>