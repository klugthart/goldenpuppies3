<?php 

$this->extend('/Common/view_sb');

$this->assign('title', 'Golden Retriever Puppies  |  Clifton Park, NY');

?>

<h2>Justice</h2>
<p>This is the perfect dog for an active family!  She has a beautiful face (broader-style) and her color is right in the middle of the range Goldens are known for. (Remember, all puppies look the same and it’s hard to predict their adult color and hair type, especially if the parents are different! Most times, the color at the tips of the puppy’s ears can be an indication.) Justice has the shorter/wavier/thicker hair that Goldens can have. Her mate has longer, straighter hair for a variety of puppies.</p>
<p>Justice has a solid pedigree, several generations on both sides. She was bread for her mellow personality and we can attest it was a success! Of course, she had her puppy stage, but she has grown into a beautiful, easy, mellow little lady! Her favorite thing to do is whatever her humans are doing - this is an incredibly social breed that bonds “quick and deep!” She loves running, walking, hiking, swimming and children! She is obsessed with children and is so patient with our toddler.</p>
<p>Justice is in great health! The cardiologist who did her breed screening to check her heart for murmurs said it was one of the best hearts he’s heard in a very long time! He complimented her many times. She has all her health checks.</p>
<p>People fall in love with Justice wherever we go. Strangers we meet on the street stop us to pet her. Friends who come to the house are greeted with kisses and she’s the perfect companion for babies and young children. She’s as gentle as they come! </p>
<h2>Micah - The Stud!</h2>
<p>Micah is owned by Jeannine and Tom and it’s safe to say they love Goldens as much as we do! They have two of their own. Jeannine says that as she got to know Micah’s personality, it was as clear as day that he would make a good father. He is so mellow and loveable! And we agree! We fell in love with him at first sight - he’s such a good boy and so handsome!</p>
<p>Micah is a very good looking and big dog with a nice broad frame and broad head. He will be a perfect compliment to Justice because she is on the small side. He has all of his health checks, as well. </p>
<p>Although Micah is very handsome and in great health, he is most known for his personality. He is extremely mellow and loveable! He is used as a therapy dog in schools where these personality traits are a must! He loves his job and the children love him!</p>
<p>Oh, and Micah even has his own website! So, be sure to check it out for more photos and details! <?= $this->Html->link(__('Click here.'),'http://kellymeadowgoldens.weebly.com/?fb_action_ids=936375683041937&fb_action_types=weeblyapp%3Ashare'); ?></p>