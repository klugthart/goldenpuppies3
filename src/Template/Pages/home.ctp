<?php 

$this->extend('/Common/view');

$this->assign('title', 'Golden Retriever Puppies  |  Clifton Park, NY');

?>

<div class="jumbotron">
	<?= $this->Html->image('puppies.png'); ?>
</div>
<div class="page-header">
	<h1><?= $this->fetch('title'); ?></h1>
</div>
<h2>About the Breed</h2>
<blockquote>
	<p>“Give your Golden Retriever puppy plenty of mental and physical activity and he'll grow up to be the best friend you ever had. This intelligent breed is beloved for their devotion, working ability and playfulness. The Golden's temperament makes him a great guide dog, search-and-rescue assistant and hunting companion. With lots of exercise and a good brushing a few times a week, your Golden Retriever will be a handsome, loving and joyful addition to the family.”</p>
	<footer><cite title="Source Title">AKC Website</cite></footer>
</blockquote>

<p class="lead">We encourage you to research this breed to know if it is the right fit for you and your lifestyle. A good place to start is at the American Kennel Association: <?= $this->Html->link(__('CLICK HERE'),'http://www.akc.org/breeds/golden_retriever/index.cfm'); ?>.</p>
<p>Wikipedia has a great summary of the breed description, statistics and history. <?= $this->Html->link(__('CLICK HERE'),'http://en.wikipedia.org/wiki/Golden_Retriever'); ?>.</p>
<blockquote><p>“Because of their intelligence, as well as their social nature, golden retrievers have been known to become sad and lonely when left alone for long periods of time by their owners. They need human interaction, in addition to canine interaction, and shouldn’t be left alone more than six or seven hours at a time. Besides becoming upset, they may even become physically ill from lack of exercise, so a daily walk and an hours worth of exercise is necessary to keep your furry friend healthy and happy. These dogs also tend to get overweight if fed too much, so make sure to feed your golden a consistent amount each day.”</p>
<footer><cite title="Source Title">Golden Retriever Facts <?= $this->Html->link(__('(click here for more)'),'https://www.udemy.com/blog/golden-retriever-facts/'); ?>.</cite></footer>
</blockquote>
