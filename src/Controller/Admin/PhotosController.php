<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Photos Controller
 *
 * @property \App\Model\Table\PhotosTable $Photos
 */
define('PHOTO_UPLOAD_PATH', WWW_ROOT.'uploads'.DS);

class PhotosController extends AppController
{

	
	public function initialize() {
		parent::initialize();
		$this->loadComponent('RequestHandler');
	}

	public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        
        $this->viewBuilder()->layout('admin');
        
        $this->Auth->deny();
    }
	
	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->paginate = [
			'contain' => ['Projects']
		];
		$this->set('photos', $this->paginate($this->Photos));
		$this->set('_serialize', ['photos']);
	}

	/**
	 * View method
	 *
	 * @param string|null $id Photo id.
	 * @return void
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function view($id = null)
	{
		$photo = $this->Photos->get($id, [
			'contain' => ['Projects', 'Comments']
		]);
		$this->set('photo', $photo);
		$this->set('_serialize', ['photo']);
	}

	/**
	 * Add method
	 *
	 * @return void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{

        // $this->layout = false;
        // debug($temp_file);
        // debug($target_filename);
        // debug($target_filepath);
		
		if(!$this->request->is('ajax')) {
	        throw new BadRequestException();
	    }

	    // debug($this->request->data);
		$photo = $this->Photos->newEntity();
		$photo['photo_file_size'] = $this->request->data['qqtotalfilesize'];
		$photo['photo_content_type'] = $this->request->data['qqfile']['type'];
		$photo['project_id'] = $this->request->data['project_id'];
		$photo['project_id'] = $this->request->data['project_id'];
		if (preg_match("/\((.*)\)/", $this->request->data['qqfilename'])) {
			// debug('INFO:: Regex not matched... '.preg_replace("/(.*)\s\((.*)\).(.*)/", "$1_$2.$3", $this->request->data['qqfilename']));
			$photo['photo'] = preg_replace("/(.*)\s\((.*)\).(.*)/", "$1_$2.$3", $this->request->data['qqfilename']);
			$success = $this->uploadFile($_FILES['qqfile']['tmp_name'], $photo);
		} else {
			// debug('INFO:: Regex matched');
			$photo['photo'] = $this->request->data['qqfilename'];
			if ($this->request->is('post')) {
				$photo = $this->Photos->patchEntity($photo, $this->request->data);
				if ($this->Photos->save($photo)) {
					$success = $this->uploadFile($_FILES['qqfile']['tmp_name'], $photo);
				} else {
					$success = false;
					// ('The photo could not be saved. Please, try again.'));
				}
			}
		}
		// debug($saveToDB.' '.$photo['photo']);
		$this->set('success', $success);
	    $this->set('_serialize', ['success']);
		// $projects = $this->Photos->Projects->find('list', ['limit' => 200]);
		// $this->set(compact('photo', 'projects'));
		// $this->set('_serialize', ['photo']);
	}

	private function uploadFile($temp_file, $photo){
        // debug($photo);
        if(!is_dir(PHOTO_UPLOAD_PATH.$photo['project_id'])) {
        	if(mkdir(PHOTO_UPLOAD_PATH.$photo['project_id'])) {
        		$this->log("Directory created!");
        	}
        }
        $target_filepath = PHOTO_UPLOAD_PATH.$photo['project_id'].DS.$photo['photo'];
        return move_uploaded_file($temp_file, $target_filepath)? true : false;
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id Photo id.
	 * @return void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{
		$photo = $this->Photos->get($id, [
			'contain' => []
		]);
		if ($this->request->is(['patch', 'post', 'put'])) {
			$photo = $this->Photos->patchEntity($photo, $this->request->data);
			if ($this->Photos->save($photo)) {
				$this->Flash->success(__('The photo has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The photo could not be saved. Please, try again.'));
			}
		}
		$projects = $this->Photos->Projects->find('list', ['limit' => 200]);
		$this->set(compact('photo', 'projects'));
		$this->set('_serialize', ['photo']);
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id Photo id.
	 * @return \Cake\Network\Response|null Redirects to index.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$photo = $this->Photos->get($id);
		if ($this->Photos->delete($photo)) {
			$this->Flash->success(__('The photo has been deleted.'));
		} else {
			$this->Flash->error(__('The photo could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
	}

	public function upload($id = null) {
		$project = $this->Photos->Projects->get($id, [
			'contain' => []
		]);
		$this->set(compact('project'));
	}
}
